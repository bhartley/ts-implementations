export class Stack {
  private maxSize: number;
  private stk: any[];

  constructor(maxSize: number) {
    if (isNaN(maxSize) || maxSize < 1) {
      this.maxSize = 1;
    } else {
      this.maxSize = maxSize;
    }

    this.stk = [];
  }

  public clear() {
    this.stk = [];
  }

  public get() {
    if (this.stk[0]) {
      return this.stk.shift();
    }
    return null;
  }

  public getStack() {
    return this.stk;
  }

  public getMaxSize() {
    return this.maxSize;
  }

  public getStackSize() {
    return this.stk.length;
  }

  public isEmpty() {
    return !this.stk[0];
  }

  public isFull() {
    return this.stk.length === this.maxSize;
  }

  public peek() {
    if (this.stk[0]) {
      return this.stk[0];
    }
    return null;
  }

  public put(item: any) {
    if (this.stk.length < this.maxSize) {
      this.stk.unshift(item);
    }
  }

}
