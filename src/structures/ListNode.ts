export class ListNode<T> {
  private data: any;
  private next?: ListNode<T> | undefined | null;
  private prev?: ListNode<T> | undefined | null;

  constructor(data: T | undefined | null) {
    this.data = data;
  }

  public getData(): any {
    return this.data;
  }

  public getNext(): ListNode<T> | undefined | null {
    return this.next;
  }

  public getPrev(): ListNode<T> | undefined | null {
    return this.prev;
  }

  public setData(data: any): void {
    this.data = data;
  }

  public setNext(node?: ListNode<T> | null): void {
    this.next = node;
  }

  public setPrev(node?: ListNode<T> | null): void {
    this.prev = node;
  }

}
