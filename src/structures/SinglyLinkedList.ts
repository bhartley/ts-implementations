import { ListNode } from "./ListNode";

export class SinglyLinkedList<T> {
  public length: number;
  public head: ListNode<T>;
  public tail: ListNode<T>;

  constructor() {
    this.length = 0;
    this.head = new ListNode<T>(null);
    this.tail = new ListNode<T>(null);
  }

  public isEmpty(): boolean {
    return this.length === 0;
  }

  public getHead() {
    return this.head;
  }

  public getTail() {
    return this.tail;
  }

  public getLength() {
    return this.length;
  }

  public pushEnd(data: T) {
    const tmp = new ListNode<T>(data);
    tmp.setNext(null);

    if (this.isEmpty()) {
      this.head = this.tail = tmp;
    } else {
      this.tail.setNext(tmp);
      this.tail = tmp;
    }
    this.length++;
  }

  public pushFront(data: T) {
    const tmp = new ListNode<T>(data);
    if (this.isEmpty()) {
      this.head = this.tail = tmp;
      this.length++;
    } else {
      tmp.setNext(this.head);
      this.head = tmp;
      this.length++;
    }
  }

  public getList(): any[] {
    const data: T[] = [];
    let tmp: any;
    tmp = this.head;
    data.push(tmp.getData());
    while (tmp.getNext()) {
      tmp = tmp.getNext();
      data.push(tmp.getData());
    }
    return data;
  }

  public search(val: any, key?: string): ListSearchResponse<T> {
    let node: any = this.head;
    const headData = node.getData();
    let depth = 0;

    if ((headData && key && headData[key] && headData[key] === val) || headData === val) {
      return { depth, node, prev: null, found: true } as ListSearchResponse<T>;
    }

    let prev: any;
    while (node.getNext()) {
      prev = node;
      node = node.getNext();
      depth++;
      const nodeData = node.getData();
      if ((nodeData && key && nodeData[key] && nodeData[key] === val) || nodeData === val) {
        return { depth, prev, node, found: true } as ListSearchResponse<T>;
      }
    }
    return { found: false } as ListSearchResponse<T>;
  }

  public searchAndDelete(val: any, key?: string): any {
    const item = this.search(val, key);
    if (typeof(item.found) === "boolean" && item.found === false) {
      return null;
    }
    const current = item.node;
    if (!current) {
      return null;
    }

    const prev = item.prev;
    const next = item.node ? item.node.getNext() : null;

    // in the middle of a list
    if (next && prev) {
      prev.setNext(next);
      current.setNext(null);
      this.length--;
    } else if (next && !prev) {
      // first item among others in a list
      current.setNext(null);
      this.head = next;
      this.length--;
    } else if (!next && prev) {
      // last item among others in a list
      prev.setNext(null);
      this.tail = prev;
      this.length--;
    } else {
      // no next, no prev = only item
      this.head = new ListNode<T>(null);
      this.tail = new ListNode<T>(null);
      this.length--;
    }

  }

  public deleteFirst(): void {
    if (this.getLength() === 0) {
      return;
    } else if (this.getLength() === 1) {
      const tmp = new ListNode<T>(null);
      this.head = this.tail = tmp;
      this.length--;
      return;
    }
    const next = this.head.getNext();
    this.head.setNext(null);
    if (next) {
      this.head = next;
    }
    this.length--;
  }

}

export interface ListSearchResponse<T> {
  depth?: number;
  found?: boolean;
  prev?: ListNode<T> | undefined | null;
  node?: ListNode<T>;
}
