#Summary
    Implementations of various data structures written in Typescript compiled into Javascript

    Work in progress, use at own risk!

    const ts = require("ts-implementations");

#CircularLinkedList
    const list = new ts.CircularLinkedList();
    list.pushEnd(1);
    list.pushEnd(2);
    list.pushEnd(3);

    const resultOne = list.insertBeforePrimitive(0, 1);
    const resultTwo = list.insertAfterPrimitive(4, 3);

    const searchResult = list.search(2);
    const deleteResult = list.searchAndDelete(2);

    const fullList = list.getList();
    const listLength = list.getLength();

    const tail = list.getTail();
    const head = list.getTail().getNext();

#DoublyLinkedList
    const list = new ts.DoublyLinkedList();
    list.pushEnd(1);
    list.pushEnd(2);
    list.pushEnd(3);

    const node = list.getHead().getNext();
    const nodeNext = node.getNext();
    const nodePrev = node.getPrev();

#GraphNode
    const g = new ts.GraphNode("data");
    const h = new ts.GraphNode("datum");
    g.connectTwoWays(h);

    const to = g.getToNodes();
    const from = g.getFromNodes();

    const i = new ts.GraphNode("datos");
    const j = new ts.GraphNode("soda");
    i.connectToOneWay(j);

#Priority Queue
    const pq = new ts.PriorityQueue();
    pq.load("red1", MAX_PRIORITY);
    pq.load("red2", MAX_PRIORITY);
    pq.load("red3", 0);
    pq.load("orange1", 1);
    pq.load("orange2", 1);
    const RED_ONE = pq.next();

#Queue
    const q = new ts.Queue();

    q.load({ data: "test" });
    q.load({ data: "testTwo" });
    q.load({ data: "testThree" });

    const view = q.view(2);
    const next = q.next();

#SinglyLinkedList
    const list = new ts.SinglyLinkedList();
    list.pushEnd(1);
    list.pushEnd(2);
    const tail = list.getTail();
    const nullNode = tail.getNext();

#Stack
    const stk = new ts.Stack(5);
    stk.put(true);
    stk.put({ toor: true });
    const get = stk.get();
